package ch08;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class TimingThreadPoolTest {
    private static final long TASK_COUNT = 2000000L;
    private static final int DEFAULT_CORE_POOL_SIZE = 4;
    private static final long DEFAULT_KEEP_ALIVE_TIME = 10L;

    @Test
    void testTaskExecutionTime_whenNotStartTheCoreThreads() throws InterruptedException {
        Map<String, String> currentHashMap = new ConcurrentHashMap<>();
        ExecutorService timingThreadPool = new TimingThreadPool(DEFAULT_CORE_POOL_SIZE, DEFAULT_KEEP_ALIVE_TIME, false);
        this.submitTasks(timingThreadPool, currentHashMap);
        timingThreadPool.shutdown();
        timingThreadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        // Terinated: finished task count=1000000, total cost=651689505ns, avg time=651ns
        // Terinated: finished task count=2000000, total cost=5743163771ns, avg time=2871ns
    }

    @Test
    void testTaskExecutionTime_whenStartTheCoreThreads() throws InterruptedException {
        Map<String, String> currentHashMap = new ConcurrentHashMap<>();
        ExecutorService timingThreadPool = new TimingThreadPool(DEFAULT_CORE_POOL_SIZE, DEFAULT_KEEP_ALIVE_TIME, true);
        this.submitTasks(timingThreadPool, currentHashMap);
        timingThreadPool.shutdown();
        timingThreadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        // Terminated: finished task count=1000000, total cost=608303779ns, avg time=608ns
        // Terminated: finished task count=2000000, total cost=2728211401ns, avg time=1364ns
    }
    private void submitTasks(ExecutorService timingThreadPool, Map<String, String> targetMap) {
        for (int i = 0; i < TASK_COUNT; i++) {
            String data = i + Thread.currentThread().getName();
            timingThreadPool.submit(() -> targetMap.put(data, data));
        }
    }

}
