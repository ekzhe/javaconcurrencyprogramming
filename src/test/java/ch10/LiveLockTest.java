package ch10;

import ch10.livelock.TaskConsumer;
import ch10.livelock.TaskProvider;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
class LiveLockTest {
    @Test
    public void CauseLiveLock_WhenConsumerExecutePoisonTasks() throws ExecutionException, InterruptedException {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        TaskProvider taskProvider = new TaskProvider();
        taskProvider.addTask(new Object());
        taskProvider.addPoison();
        taskProvider.addTask(new Object());
        Object task = taskProvider.pollTask();
        TaskConsumer customer = new TaskConsumer();
        while (task != null) {
            Object finalTask = task;
            singleThreadExecutor.submit(() -> customer.executeTask(finalTask, taskProvider::addToFirst));
            task = singleThreadExecutor.submit(taskProvider::pollTask).get();
        }
        System.out.println("SingleThreadExecutor execute all task end...");
    }

    @Test
    public void NoLiveLock_WhenConsumerExecuteNormalTasks() throws ExecutionException, InterruptedException {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        TaskProvider taskProvider = new TaskProvider();
        taskProvider.addTask(new Object());
        taskProvider.addTask(new Object());
        taskProvider.addTask(new Object());
        Object task = taskProvider.pollTask();
        TaskConsumer customer = new TaskConsumer();
        while (task != null) {
            Object finalTask = task;
            singleThreadExecutor.submit(() -> customer.executeTask(finalTask, taskProvider::addToFirst));
            task = singleThreadExecutor.submit(taskProvider::pollTask).get();
        }
        System.out.println("SingleThreadExecutor execute all task end...");
    }

}
