package ch10;

import ch10.LeftRightDeadLock.Customer;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class LockOrderDeadLockTest {
    @Test
    public void NoDeadLock_WhenTwoConsumerBothWithRightLeftLockingOrder() throws InterruptedException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        LeftRightDeadLock leftRightDeadLock = new LeftRightDeadLock();
        Customer customerA = new Customer(leftRightDeadLock);
        Customer customerB = new Customer(leftRightDeadLock);
        System.out.println("Executor run task start...");
        executor.execute(customerA::rightLeftOrder);
        executor.execute(customerB::rightLeftOrder);
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        System.out.println("Executor run task end...");
    }

    @Test
    public void NoDeadLock_WhenTwoConsumerBothWithLeftRightLockingOrder() throws InterruptedException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        LeftRightDeadLock leftRightDeadLock = new LeftRightDeadLock();
        Customer customerA = new Customer(leftRightDeadLock);
        Customer customerB = new Customer(leftRightDeadLock);
        System.out.println("Executor run task start...");
        executor.submit(customerA::leftRightOrder);
        executor.submit(customerB::leftRightOrder);
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        System.out.println("Executor run task end...");
    }

    @Test
    public void CauseDeadLock_WhenTwoConsumerWithReverseLockingOrder() throws InterruptedException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        LeftRightDeadLock leftRightDeadLock = new LeftRightDeadLock();
        Customer customerA = new Customer(leftRightDeadLock);
        Customer customerB = new Customer(leftRightDeadLock);
        System.out.println("Executor run task start...");
        executor.submit(customerA::rightLeftOrder);
        executor.submit(customerB::leftRightOrder);
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        System.out.println("Executor run task end...");
    }

}
