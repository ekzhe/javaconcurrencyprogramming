package ch07.homework;

import ch03.ImprovedMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// The CauseDeadLock_WhenTwoCustomerUseLock task design may not be perfect and stable
class ImprovedTestHarnessV2Test {
    public static final int TASK_COUNT = 1000;
    public static final int THREAD_COUNT = 2;

    @Test
    void shouldReturnPositiveNumberResult_WhenTheTaskIsNormalEnd() {
        Map<String, String> currentHashMap = new ConcurrentHashMap<>();
        Map<String, String> improvedMap = new ImprovedMap<>();
        ImprovedTestHarnessV2 improvedTestHarness = new ImprovedTestHarnessV2();
        long currentHashMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, 3, putElements(TASK_COUNT,
                currentHashMap));
        long improvedMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, 3, putElements(TASK_COUNT,
                improvedMap));
        System.out.println("==================== ImprovedTestHarnessV2 HarnessTest ======================");
        System.out.printf("[%d] Threads put [%d] elements, currentHashMap  cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, currentHashMapCostByImproved);
        System.out.printf("[%d] Threads put [%d] elements, improvedMap     cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, improvedMapCostByImproved);
    }

    @Test
    void shouldReturnNegativeOne__WhenTaskExecCalculateTimeOut() {
        // Given a ImprovedTestHarnessV2 Object
        ImprovedTestHarnessV2 improvedTestHarness = new ImprovedTestHarnessV2();
        // When invoke the timeTasks with 2 threads , 1 seconds time out and 1000000 count
        long currentHashMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, 1,
                putElements(TASK_COUNT * 1000, new ConcurrentHashMap<>()));
        // Then verify the result is -1 witch is meaning the task timed out
        Assertions.assertEquals(-1, currentHashMapCostByImproved);
    }
    private Runnable putElements(int taskCount, Map<String, String> targetMap) {
        return () -> IntStream.range(0, taskCount)
                .forEach(itemsNo -> targetMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(),
                        String.format("%09d", itemsNo) + Thread.currentThread().getName()));
    }

}
