package ch07.bookcode;

import java.math.BigInteger;
import java.util.List;
import org.junit.jupiter.api.Test;

class PrimeGeneratorTest {

    @Test
    void testOneSecond() throws InterruptedException {
        PrimeGenerator primeGenerator = new PrimeGenerator();
        List<BigInteger> bigIntegers = primeGenerator.generatePrimes();
        System.out.println(bigIntegers);
    }

}
