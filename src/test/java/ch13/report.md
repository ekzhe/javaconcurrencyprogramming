# Benkmark Test

## Test Environment
 - JMH version: 1.35
 - VM version: JDK 11.0.19, OpenJDK 64-Bit Server VM, 11.0.19+7-LTS
## Test Parameters
Warmup: 1 iterations, 10 s each
 - Measurement: 3 iterations, 10 s each
 - Timeout: 10 min per iteration
 - Threads: 5 threads, will synchronize iterations
 - Benchmark mode: Throughput, ops/time
## Test Case
- Put 2 elements to the fruit and vegetable grocery each time

##  Grocery Test Result
### FairReentrantLockGroceryTest

- 2742.688 ±(99.9%) 11867.608 ops/s [Average]
- (min, avg, max) = (2188.009, 2742.688, 3458.665), stdev = 650.504
- CI (99.9%): [≈ 0, 14610.296] (assumes normal distribution)

### SingleLockGroceryTest

- 4957.450 ±(99.9%) 36733.492 ops/s [Average]
- (min, avg, max) = (3019.525, 4957.450, 7038.826), stdev = 2013.487
- CI (99.9%): [≈ 0, 41690.943] (assumes normal distribution)

### SynchronizedGroceryTest
- 4322.336 ±(99.9%) 32451.383 ops/s [Average]
- (min, avg, max) = (2807.852, 4322.336, 6281.150), stdev = 1778.770
- CI (99.9%): [≈ 0, 36773.718] (assumes normal distribution)

### UnfairReentrantLockGroceryTest
- 2468.028 ±(99.9%) 10507.511 ops/s [Average]
- (min, avg, max) = (1976.262, 2468.028, 3101.656), stdev = 575.952
- CI (99.9%): [≈ 0, 12975.540] (assumes normal distribution)

## Benchmark Summary

| Benchmark                                               | Mode  | Cnt | Score    | Error       | Units |
|---------------------------------------------------------|-------|-----|----------|-------------|-------|
| Ch13GroceryBenchmarkTest.fairReentrantLockGroceryTest   | thrpt | 3   | 2742.688 | ± 11867.608 | ops/s |
| Ch13GroceryBenchmarkTest.singleLockGroceryTest          | thrpt | 3   | 4957.450 | ± 36733.492 | ops/s |
| Ch13GroceryBenchmarkTest.synchronizedGroceryTest        | thrpt | 3   | 4322.336 | ± 32451.383 | ops/s | 
| Ch13GroceryBenchmarkTest.unfairReentrantLockGroceryTest | thrpt | 3   | 2468.028 | ± 10507.511 | ops/s |

---

# Test for fair and unfair
## Test Case
- Put 100 elements to the fruit and vegetable grocery each time
## Test  Parameter
- Warmup: 1 iterations, 10 s each
- Measurement: 2 iterations, 10 s each
- Timeout: 10 min per iteration
- Threads: 20 threads, will synchronize iterations
- Benchmark mode: Throughput, ops/time

## Result

| Benchmark                                               | Mode  | Cnt | Score  | Error | Units |
|---------------------------------------------------------|-------|-----|--------|-------|-------|
| Ch13GroceryBenchmarkTest.fairReentrantLockGroceryTest   | thrpt | 2   | 52.272 |       | ops/s |
| Ch13GroceryBenchmarkTest.unfairReentrantLockGroceryTest | thrpt | 2   | 56.109 |       | ops/s |

# Function Test for ReentrantLockGrocery
## Test Case
- 10 worker
- put 100000 elements to the fruit and vegetable grocery each time
## Test Result

| Class                      | Cost        |
|----------------------------|-------------|
| FairReentrantLockGrocery   | 17368191684 |
| UnfairReentrantLockGrocery | 16759146457 |

