package ch13;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@OutputTimeUnit(TimeUnit.SECONDS)
@Threads(5)
@Fork(1)
public class Ch13GroceryBenchmarkTest {
    public static final int FOOD_COUNT = 2;
    @State(Scope.Benchmark)
    public static class Factory {
        final Grocery grocery = new SynchronizedGrocery();
        final Grocery singleLockGrocery = new SingleLockGrocery();
        final Grocery unfairReentrantLockGrocery = new ReentrantLockGrocery(false);
        final Grocery fairReentrantLockGrocery = new ReentrantLockGrocery(true);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(Ch13GroceryBenchmarkTest.class.getSimpleName()).build();
        new Runner(options).run();
    }

    @Benchmark
    public void synchronizedGroceryTest(Factory factory) {
        addFood(factory.grocery);
    }

    @Benchmark
    public void singleLockGroceryTest(Factory factory) {
        addFood(factory.singleLockGrocery);
    }

    @Benchmark
    public void unfairReentrantLockGroceryTest(Factory factory) {
        addFood(factory.unfairReentrantLockGrocery);
    }
    @Benchmark
    public void fairReentrantLockGroceryTest(Factory factory) {
        addFood(factory.fairReentrantLockGrocery);
    }
    private void addFood(Grocery grocery) {
        for (int i = 0; i < FOOD_COUNT; i++) {
            grocery.addFruit(i, Thread.currentThread().getName() + i);
            grocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }

}
