package ch11;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 1)
@Measurement(iterations = 3)
@OutputTimeUnit(TimeUnit.SECONDS)
@Threads(5)
@Fork(1)
public class GroceryBenchmarkTest {
    public static final int FOOD_COUNT = 2;
    @State(Scope.Benchmark)
    public static class Factory {
        final Grocery grocery = new SynchronizedGrocery();
        final Grocery singleLockGrocery = new SingleLockGrocery();
        final Grocery vectorGrocery = new VectorGrocery();
        final Grocery synchronizedCollectionGrocery = new SynchronizedCollectionGrocery();
        final Grocery copyOnWriteGrocery = new CopyOnWriteGrocery();
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(GroceryBenchmarkTest.class.getSimpleName()).build();
        new Runner(options).run();
    }

    @Benchmark
    public void synchronizedGrocery(Factory factory) {
        addFood(factory.grocery);
    }

    @Benchmark
    public void vectorGrocery(Factory factory) {
        addFood(factory.vectorGrocery);
    }

    @Benchmark
    public void singleLockGrocery(Factory factory) {
        addFood(factory.singleLockGrocery);
    }

    @Benchmark
    public void synchronizedCollectionGrocery(Factory factory) {
        addFood(factory.synchronizedCollectionGrocery);
    }
    @Benchmark
    public void copyOnWriteGrocery(Factory factory) {
        addFood(factory.copyOnWriteGrocery);
    }
    private void addFood(Grocery grocery) {
        for (int i = 0; i < FOOD_COUNT; i++) {
            grocery.addFruit(i, Thread.currentThread().getName() + i);
            grocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }

}
