package ch11;

import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FoodSupplierTest {
    public static final int FOOD_COUNT = 100000;
    public static final int WORK_COUNT = 10;
    private FoodSupplier foodSupplier;

    @BeforeEach
    void init() {
        foodSupplier = new FoodSupplier(WORK_COUNT);
    }

    @Test
    public void testSynchronizedGrocery() throws ExecutionException, InterruptedException {
        Grocery grocery = new SynchronizedGrocery();
        sendFoodToGrocery(foodSupplier, grocery);
    }

    @Test
    public void testSingleLockGrocery() throws ExecutionException, InterruptedException {
        Grocery grocery = new SingleLockGrocery();
        sendFoodToGrocery(foodSupplier, grocery);
    }

    @Test
    public void testSynchronizedCollectionGrocery() throws ExecutionException, InterruptedException {
        Grocery grocery = new SynchronizedCollectionGrocery();
        sendFoodToGrocery(foodSupplier, grocery);
    }

    @Test
    public void testVectorGrocery() throws ExecutionException, InterruptedException {
        Grocery grocery = new VectorGrocery();
        sendFoodToGrocery(foodSupplier, grocery);
    }

    @Test
    public void testCopyOnWriteGrocery() throws ExecutionException, InterruptedException {
        Grocery grocery = new CopyOnWriteGrocery();
        sendFoodToGrocery(foodSupplier, grocery);
    }

    private static void sendFoodToGrocery(FoodSupplier foodSupplier, Grocery grocery) throws ExecutionException, InterruptedException {
        long start = System.nanoTime();
        foodSupplier.addFoodToGrocery(FOOD_COUNT, grocery);
        foodSupplier.shutdown();
        System.out.printf("[%s] cost: [%d]%n", grocery.getClass().getName(), System.nanoTime() - start);
    }

}
