# Report

## Traditional

### 4 threads 100000 tasks

| name                          | time        |
| ----------------------------- | ----------- |
| SynchronizedGrocery           | 3391301100  |
| SynchronizedCollectionGrocery | 1541521800  |
| CopyOnWriteGrocery            | 34056698300 |
| VectorGrocery                 | 1726752800  |
| SingleLockGrocery             | 1589568000  |

### 10 threads 100000 tasks

| name                          | time        |
| ----------------------------- | ----------- |
| SynchronizedGrocery           | 3524749100  |
| SynchronizedCollectionGrocery | 1657149600  |
| CopyOnWriteGrocery            | 41996751300 |
| VectorGrocery                 | 1316593500  |
| SingleLockGrocery             | 1456392600  |


## JMH

### 2 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 3629.512 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 8597.586 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 8915.388 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 9096.670 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 9474.556 | ops/s |

### 5 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 4048.701 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 9987.116 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 9969.230 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 9895.191 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 9967.094 | ops/s |

### 10 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 3749.662 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 9058.104 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 8720.690 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 8062.919 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 8126.521 | ops/s |
