package ch13;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockGrocery implements Grocery {
    private final ReentrantLock reentrantLock;
    public ReentrantLockGrocery(boolean fair) {
        reentrantLock = new ReentrantLock(fair);
    }

    private final List<String> fruits = new CopyOnWriteArrayList<>();
    private final List<String> vegetables = new CopyOnWriteArrayList<>();

    public void addFruit(int index, String fruit) {
        reentrantLock.lock();
        try {
            fruits.add(index, fruit);
        } finally {
            reentrantLock.unlock();
        }
    }

    public void addVegetable(int index, String vegetable) {
        reentrantLock.lock();
        try {
            vegetables.add(index, vegetable);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override
    public int fruitSize() {
        return fruits.size();
    }

    @Override
    public int vegetableSize() {
        return vegetables.size();
    }

}
