package ch13;

public interface Grocery {

    void addFruit(int index, String fruit);

    void addVegetable(int index, String vegetable);

    int fruitSize();

    int vegetableSize();
}
