# Test Report

## HarnessTest

- [10] Threads put [10000] elements, synchronizedMap cost :[442516000]
- [10] Threads put [10000] elements, currentHashMap  cost :[293877000]
- [10] Threads put [10000] elements, improvedMap     cost :[275828200]

## Improved HarnessTest

- [10] Threads put [10000] elements, synchronizedMap cost :[221128300]
- [10] Threads put [10000] elements, currentHashMap  cost :[157809600]
- [10] Threads put [10000] elements, improvedMap     cost :[135047300]

## Summary

- The consistency of startup times is controlled through the last Barrier of the main thread.
- Thread Pools reduce thread startup destruction and improve performance.

