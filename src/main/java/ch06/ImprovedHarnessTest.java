package ch06;

import ch03.ImprovedMap;
import ch05.TestHarness;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

public class ImprovedHarnessTest {
    public static final int TASK_COUNT = 10000;
    public static final int THREAD_COUNT = 10;

    public static void main(String[] args) throws Exception {

        Map<String, String> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
        Map<String, String> currentHashMap = new ConcurrentHashMap<>();
        Map<String, String> improvedMap = new ImprovedMap<>();
        TestHarness testHarness = new TestHarness();
        long synchronizedMapCost = testHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT, synchronizedMap));
        long currentHashMapCost = testHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT, currentHashMap));
        long improvedMapCost = testHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT, improvedMap));
        System.out.println("==================== HarnessTest ======================");
        System.out.printf("[%d] Threads put [%d] elements, synchronizedMap cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, synchronizedMapCost);
        System.out.printf("[%d] Threads put [%d] elements, currentHashMap  cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, currentHashMapCost);
        System.out.printf("[%d] Threads put [%d] elements, improvedMap     cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, improvedMapCost);

        TestHarness improvedTestHarness = new ImprovedTestHarness();
        long synchronizedMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT,
                synchronizedMap));
        long currentHashMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT, currentHashMap));
        long improvedMapCostByImproved = improvedTestHarness.timeTasks(THREAD_COUNT, putElements(TASK_COUNT, improvedMap));
        System.out.println("==================== Improved HarnessTest ======================");
        System.out.printf("[%d] Threads put [%d] elements, synchronizedMap cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, synchronizedMapCostByImproved);
        System.out.printf("[%d] Threads put [%d] elements, currentHashMap  cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, currentHashMapCostByImproved);
        System.out.printf("[%d] Threads put [%d] elements, improvedMap     cost :[%d]%n",
                THREAD_COUNT, TASK_COUNT, improvedMapCostByImproved);
    }

    private static Runnable putElements(int taskCount, Map<String, String> targetMap) {
        return () -> IntStream.range(0, taskCount)
                .forEach(itemsNo -> targetMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(),
                        String.format("%09d", itemsNo) + Thread.currentThread().getName()));
    }

}
