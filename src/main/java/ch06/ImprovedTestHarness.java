package ch06;

import ch05.TestHarness;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImprovedTestHarness extends TestHarness {
    @Override
    public long timeTasks(int nThreads, final Runnable task) throws Exception {

        final ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        final CyclicBarrier startBarrier = new CyclicBarrier(nThreads + 1);
        final CyclicBarrier endBarrier = new CyclicBarrier(nThreads + 1);

        for (int i = 0; i < nThreads; i++) {
            executorService.submit(() -> {
                try {
                    try {
                        startBarrier.await();
                        task.run();
                    } finally {
                        endBarrier.await();
                    }
                } catch (InterruptedException | BrokenBarrierException ignored) {
                    // ignored
                }
            });
        }

        startBarrier.await();
        long start = System.nanoTime();
        endBarrier.await();
        long end = System.nanoTime();
        executorService.shutdown();
        return end - start;

    }
}
