package ch11;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

public class FoodSupplier {

    private final ExecutorService fruitWorker;
    private final ExecutorService vegetableWorker;

    public FoodSupplier(int count) {
        this.fruitWorker = new ThreadPoolExecutor(count, count, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        this.vegetableWorker = new ThreadPoolExecutor(count, count, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
    }

    public void addFoodToGrocery(long count, Grocery grocery) throws ExecutionException, InterruptedException {
        submitTask(count, grocery::addFruit);
        submitTask(count, grocery::addVegetable);
    }

    public boolean shutdown() throws InterruptedException {
        fruitWorker.shutdown();
        vegetableWorker.shutdown();
        return fruitWorker.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS) && vegetableWorker.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
    }

    private void submitTask(long count, BiConsumer<Integer,String> consumer) {
        fruitWorker.submit(() -> {
            for (long i = 0; i < count; i++) {
                consumer.accept((int) i, Thread.currentThread().getName() + i);
            }
        });
        vegetableWorker.submit(() -> {
            for (long i = 0; i < count; i++) {
                consumer.accept((int) i, Thread.currentThread().getName() + i);
            }
        });
    }
}
