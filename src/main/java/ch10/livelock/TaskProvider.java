package ch10.livelock;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

public class TaskProvider {
    static final Object poisonTask = new Object();
    private final Deque<Object> taskQueue = new LinkedBlockingDeque<>();

    public boolean addTask(Object task) {
        return taskQueue.add(task);
    }

    public boolean addPoison() {
        return taskQueue.add(poisonTask);
    }

    public Object pollTask() {
        return taskQueue.pollFirst();
    }
    public void addToFirst(Object task) {
        taskQueue.offerFirst(task);
    }

}

