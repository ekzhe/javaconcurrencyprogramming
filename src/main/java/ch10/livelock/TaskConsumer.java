package ch10.livelock;

import java.util.function.Consumer;

public class TaskConsumer {
    public void executeTask(Object task, Consumer<Object> poisonFunc) {
        if (TaskProvider.poisonTask.equals(task)) {
            // mock business processing
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("oh, find a poison task...");
            poisonFunc.accept(task);
            return;
        }
        // mock business processing
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.printf("[%s] task has been executed %n", task.hashCode());
    }
}
