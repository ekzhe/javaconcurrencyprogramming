package ch10;
public class LeftRightDeadLock {
    private final Object leftLock = new Object();
    private final Object rightLock = new Object();

    static class Customer {
        private final LeftRightDeadLock leftRightDeadLock;

        public Customer(LeftRightDeadLock lockOrderDeadLock) {
            this.leftRightDeadLock = lockOrderDeadLock;
        }

        public void leftRightOrder() {
            synchronized (leftRightDeadLock.leftLock) {
                // mock business processing
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (leftRightDeadLock.rightLock) {
                    System.out.println("left right order task end...");
                }
            }
        }
        public void rightLeftOrder() {
            synchronized (leftRightDeadLock.rightLock) {
                // mock business processing
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (leftRightDeadLock.leftLock) {
                    System.out.println("right left order task end...");
                }
            }
        }
    }

}
