package ch07.homework;
public interface TimeoutTestHarness {

    public long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task);

}


