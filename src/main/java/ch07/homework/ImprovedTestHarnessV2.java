package ch07.homework;

import ch03.ImprovedMap;
import ch05.TestHarness;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

// Log4j will replace system.out as product code
public class ImprovedTestHarnessV2 extends TestHarness implements TimeoutTestHarness {
    @Override
    public long timeTasks(int nThreads, int timeoutInSeconds, Runnable task) {
        AtomicLong cost = new AtomicLong(0);
        final ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        List<CompletableFuture<Long>> childTaskFuture = new ArrayList<>();
        for (int i = 0; i < nThreads; i++) {
            CompletableFuture<Long> taskFuture = CompletableFuture.supplyAsync(() -> {
                long start = System.nanoTime();
                task.run();
                long end = System.nanoTime();
                return end - start;
            }, executorService).whenCompleteAsync((result, throwable) -> {
                if (result != null) {
                    System.out.printf("current task cost: [%d]%n", result);
                    cost.addAndGet(result);
                }
                if (throwable != null) {
                    throw new RuntimeException(throwable);
                }
            });
            childTaskFuture.add(taskFuture);
        }
        CompletableFuture<?>[] futuresArray = childTaskFuture.toArray(new CompletableFuture[0]);
        CompletableFuture<Void> allFuture = null;
        try {
            allFuture = CompletableFuture.allOf(futuresArray);
            allFuture.get(timeoutInSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            System.out.println("Task Time Out!!!");
            allFuture.cancel(true);
            cost.addAndGet(-1);
            Thread.currentThread().interrupt();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        } finally {
            executorService.shutdown();
        }
        if (allFuture.isCancelled()) {
            System.out.println("Time Test Time Out End");
        } else {
            System.out.println("Time Test Task End");
        }
        return cost.get();
    }
}
