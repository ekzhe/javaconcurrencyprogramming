package ch07.bookcode;

import java.math.BigInteger;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//  Cancel by interruption
public class ImprovedBrokenPrimeProducer extends Thread {

    private final BlockingQueue<BigInteger> queue;

    public ImprovedBrokenPrimeProducer(BlockingQueue<BigInteger> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            BigInteger p = BigInteger.ONE;
            while (!Thread.currentThread().isInterrupted()) {
                queue.put(p = p.nextProbablePrime());
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void cancel() {
        interrupt();
    }

    void consumePrimes() throws InterruptedException {
        BlockingQueue<BigInteger> primes = new LinkedBlockingQueue<>();
        ImprovedBrokenPrimeProducer primeProducer = new ImprovedBrokenPrimeProducer(primes);
        primeProducer.start();
        try {
            while (true) {
                primes.take();
            }
        } finally {
            primeProducer.cancel();
        }
    }

}
