package ch07.bookcode;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CancelByFuture {

    static ExecutorService taskExec = Executors.newFixedThreadPool(4);

    public static void timedRun(Runnable r, long timeout, TimeUnit unit) throws InterruptedException {
        Future<?> task = taskExec.submit(r);
        try {
            task.get(timeout, unit);
        } catch (TimeoutException e) {
            // cancel the task
        } catch (ExecutionException e) {
            // If an exception is thrown in the task, the exception is rethrown
            throw new RuntimeException(e.getCause());
        } finally {
            task.cancel(true);
        }
    }

}
