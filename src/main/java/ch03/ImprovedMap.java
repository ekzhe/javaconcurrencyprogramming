package ch03;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

public class ImprovedMap<K, V> implements Map<K, V> {
    private final Object lock = new Object();
    private final Map<K, V> innerMap;

    public ImprovedMap() {
        innerMap = new HashMap<>();
    }

    public V put(K key, V value) {
        synchronized (lock) {
            innerMap.put(key, value);
            return value;
        }
    }

    @Override
    public V remove(Object key) {
        synchronized (lock) {
            return innerMap.remove(key);
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        synchronized (lock) {
            return innerMap.keySet();
        }
    }

    @Override
    public Collection<V> values() {
        synchronized (lock) {
            return innerMap.values();
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        synchronized (lock) {
            return innerMap.entrySet();
        }
    }

    public V get(Object key) {
        synchronized (lock) {
            return innerMap.get(key);
        }
    }

    public int size() {
        return innerMap.size();
    }

    @Override
    public boolean isEmpty() {
        synchronized (lock) {
            return innerMap.isEmpty();
        }
    }

    @Override
    public boolean containsKey(Object key) {
        synchronized (lock) {
            return innerMap.containsKey(key);
        }
    }

    @Override
    public boolean containsValue(Object value) {
        synchronized (lock) {
            return innerMap.containsValue(value);
        }
    }

    public static void main(String[] args) {
        ImprovedMap<Integer, Integer> improvedMap = new ImprovedMap<>();
        Map<Integer, Integer> map = new HashMap<>();
        System.out.println("Task Start...");
        IntStream.range(0, 10000).parallel().forEach(number -> {
            map.put(number, number);
            improvedMap.put(number, number);
        });
        System.out.println("Task End...");
        System.out.printf("The safeMap final size is [%d]. %n",
                improvedMap.size());
        System.out.printf("The gap between safeMap size and map size is [%d].",
                improvedMap.size() - map.size());
    }
}


