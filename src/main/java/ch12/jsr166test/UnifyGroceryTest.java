package ch12.jsr166test;

import static ch12.jsr166test.JSR166TestCase.nine;
import static ch12.jsr166test.JSR166TestCase.zero;

import ch12.jsr166test.JSR166TestCase.SimpleThreadFactory;
import ch12.jsr166test.specific.CopyOnWriteGroceryTest;
import ch12.jsr166test.specific.SingleLockGroceryTest;
import ch12.jsr166test.specific.SynchronizedCollectionGroceryTest;
import ch12.jsr166test.specific.SynchronizedGroceryTest;
import ch12.jsr166test.specific.VectorGroceryTest;
import ch12.pojo.Grocery;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class UnifyGroceryTest extends TestCase {

    protected Grocery grocery = null;

    static TestSuite suite() {
        Class<?>[] testClasses = {
            CopyOnWriteGroceryTest.class,
            SingleLockGroceryTest.class,
            SynchronizedGroceryTest.class,
            SynchronizedCollectionGroceryTest.class,
            VectorGroceryTest.class,
        };
        return new TestSuite(testClasses);
    }

    public void testAddFruitMethodInSingleThread() {
        int fruteSize = nine;
        int vegetableExpectedSize = zero;
        for (int i = 0; i < fruteSize; i++) {
            grocery.addFruit(i, String.valueOf(i));
        }
        assertEquals(fruteSize, grocery.fruitSize());
        assertEquals(vegetableExpectedSize, grocery.vegetableSize());
    }

    public void testAddVegetableMethodInSingleThread() {
        int vegetable = nine;
        int fruitExpectedSize = zero;
        for (int i = 0; i < vegetable; i++) {
            grocery.addVegetable(i, String.valueOf(i));
        }
        assertEquals(vegetable, grocery.vegetableSize());
        assertEquals(fruitExpectedSize, grocery.fruitSize());
    }

    public void testAddFruitMethodInMultiThread() throws InterruptedException {
        SimpleThreadFactory simpleThreadFactory = new SimpleThreadFactory();
        Thread thread1 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addFruit(i, String.valueOf(i));
            }
        });
        Thread thread2 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addFruit(i, String.valueOf(i));
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        assertEquals(20000, grocery.fruitSize());
        assertEquals(0, grocery.vegetableSize());
    }

    public void testAddVegetableMethodInMultiThread() throws InterruptedException {
        SimpleThreadFactory simpleThreadFactory = new SimpleThreadFactory();
        Thread thread1 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addVegetable(i, String.valueOf(i));
            }
        });
        Thread thread2 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addVegetable(i, String.valueOf(i));
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        assertEquals(20000, grocery.vegetableSize());
        assertEquals(0, grocery.fruitSize());
    }
}
