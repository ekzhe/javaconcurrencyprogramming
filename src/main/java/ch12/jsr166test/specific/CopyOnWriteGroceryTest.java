package ch12.jsr166test.specific;

import ch12.jsr166test.UnifyGroceryTest;
import ch12.pojo.CopyOnWriteGrocery;

public class CopyOnWriteGroceryTest extends UnifyGroceryTest {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.grocery = new CopyOnWriteGrocery();
        System.out.printf("{%s} Test Setup Method Invoked.%n",CopyOnWriteGrocery.class);
    }
}
