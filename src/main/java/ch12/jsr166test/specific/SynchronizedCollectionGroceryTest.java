package ch12.jsr166test.specific;

import ch12.jsr166test.UnifyGroceryTest;
import ch12.pojo.SynchronizedCollectionGrocery;

public class SynchronizedCollectionGroceryTest extends UnifyGroceryTest {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.grocery = new SynchronizedCollectionGrocery();
        System.out.printf("{%s} Test Setup Method Invoked.%n",SynchronizedCollectionGroceryTest.class);
    }
}
