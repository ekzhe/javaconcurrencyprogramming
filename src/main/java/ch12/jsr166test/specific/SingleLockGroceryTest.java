package ch12.jsr166test.specific;

import ch12.jsr166test.UnifyGroceryTest;
import ch12.pojo.SingleLockGrocery;

public class SingleLockGroceryTest extends UnifyGroceryTest {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.grocery = new SingleLockGrocery();
        System.out.printf("{%s} Test Setup Method Invoked.%n",SingleLockGroceryTest.class);
    }
}
