# Report

## JSR 166 Function Test

- Integrate the `JSR166TestCase` of the guava, read the source code and related doc.
- Create BaseTestClass and 5 ChildClass.
- Add an getSize method to verify the add result.
- Add TestCase to verify the correctness of the `addFruit` and `addVegetable` method in single and multi threads environments.

![function_test](function_test.png)


## JMH

### 2 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 3629.512 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 8597.586 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 8915.388 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 9096.670 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 9474.556 | ops/s |

### 5 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 4048.701 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 9987.116 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 9969.230 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 9895.191 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 9967.094 | ops/s |

### 10 threads

| Benchmark                                          | Mode  | Score    | Units |
| -------------------------------------------------- | ----- | -------- | ----- |
| GroceryBenchmarkTest.copyOnWriteGrocery            | thrpt | 3749.662 | ops/s |
| GroceryBenchmarkTest.singleLockGrocery             | thrpt | 9058.104 | ops/s |
| GroceryBenchmarkTest.synchronizedCollectionGrocery | thrpt | 8720.690 | ops/s |
| GroceryBenchmarkTest.synchronizedGrocery           | thrpt | 8062.919 | ops/s |
| GroceryBenchmarkTest.vectorGrocery                 | thrpt | 8126.521 | ops/s |
