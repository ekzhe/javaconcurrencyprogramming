package ch12;


import ch12.pojo.CopyOnWriteGrocery;
import ch12.pojo.Grocery;
import ch12.pojo.SingleLockGrocery;
import ch12.pojo.SynchronizedGrocery;
import ch12.pojo.VectorGrocery;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@Fork(1)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 0)
@Measurement(iterations = 1)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Thread)
public class GroceryBenchmarkTest {
    public static final int FOOD_COUNT = 50000;
    static Grocery grocery = null;
    static Grocery copyOnWriteGrocery = null;
    static Grocery vectorGrocery = null;
    static Grocery singleLockGrocery = null;

    @Setup
    public static void initGrocery() {
        grocery = new SynchronizedGrocery();
        copyOnWriteGrocery = new CopyOnWriteGrocery();
        vectorGrocery = new VectorGrocery();
        singleLockGrocery = new SingleLockGrocery();
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(GroceryBenchmarkTest.class.getSimpleName()).build();
        new Runner(options).run();
    }

    @Benchmark
    public void synchronizedGroceryTest() {
        for (int i = 0; i < FOOD_COUNT; i++) {
            grocery.addFruit(i, Thread.currentThread().getName() + i);
            grocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }

    @Benchmark
    public void copyOnWriteGroceryTest() {
        for (int i = 0; i < FOOD_COUNT; i++) {
            copyOnWriteGrocery.addFruit(i, Thread.currentThread().getName() + i);
            copyOnWriteGrocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }

    @Benchmark
    public void vectorGroceryTest() {
        for (int i = 0; i < FOOD_COUNT; i++) {
            vectorGrocery.addFruit(i, Thread.currentThread().getName() + i);
            vectorGrocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }
    @Benchmark
    public void singleLockGroceryTest() {
        for (int i = 0; i < FOOD_COUNT; i++) {
            vectorGrocery.addFruit(i, Thread.currentThread().getName() + i);
            vectorGrocery.addVegetable(i, Thread.currentThread().getName() + i);
        }
    }

}
