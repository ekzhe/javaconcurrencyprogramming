package ch08;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

public class TimingThreadPool extends ThreadPoolExecutor {
    private final ThreadLocal<Long> startTime = new ThreadLocal<>();
    private static final Logger log = Logger.getLogger("TimingThreadPool");
    private final AtomicLong numTasks = new AtomicLong();
    private final AtomicLong totalTime = new AtomicLong();

    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;

    public TimingThreadPool(int corePoolSize, long keepAliveTime, boolean shouldStartAllCoreThreads) {
        super(corePoolSize, corePoolSize, keepAliveTime, DEFAULT_TIME_UNIT, new LinkedBlockingQueue<>());
        int recommendedMaximumSize = Runtime.getRuntime().availableProcessors() + 1;
        int maximumPoolSize = Math.max(recommendedMaximumSize, corePoolSize);
        super.setMaximumPoolSize(maximumPoolSize);
        if (shouldStartAllCoreThreads) {
            int coreThreadsCount = super.prestartAllCoreThreads();
            log.info(String.format("[%d] Core Threads have prestarted.", coreThreadsCount));
        }
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        log.fine(String.format("Thread %s: start %s", t.getName(), r));
        startTime.set(System.nanoTime());
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        try {
            long endTime = System.nanoTime();
            long taskTime = endTime - startTime.get();
            numTasks.incrementAndGet();
            totalTime.addAndGet(taskTime);
            log.fine(String.format("Thread %s: end, time=[%dns]", Thread.currentThread().getName(), taskTime));
        } finally {
            super.afterExecute(r, t);
        }

    }

    @Override
    protected void terminated() {
        try {
            log.info(
                String.format("Terinated: finished task count=%d, total cost=%dns, avg time=%dns", numTasks.get(),
                    totalTime.get(), totalTime.get() / numTasks.get()));
        } finally {
            super.terminated();
        }
    }
}

